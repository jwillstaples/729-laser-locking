### constant_test is meant to use labjack and wlm integreation to determine locking constants
### Author: Will Staples
### July 29, 2021

from lock_main import LabjackMonitor, WlmMonitor
from time import sleep
import matplotlib.pyplot as plt

if __name__ == "__main__":

    lm = LabjackMonitor()
    wm = WlmMonitor()

    # *** Determination of K constant ***
    """
    To determine the K constant it is necessary to find a linear approximation of frequency in terms of applied voltage. 
    In order to do this, measure the initial voltage— v0 —then vary slightly in each direction and record frequency from 
    the wlm. Outside software will be used for data processing.
    """
    # Attach AIN0 to terminal
    v0 = lm.read_ch2() # note that ch2 is hardcoded as attatched to AIN0

    # Attach DAC to terminal
    wl_list = []
    for i in range(-0.1, 0.1, 0.01):
        lm.write_dac(v0 + i)
        sleep(0.1)
        wl_list.append(wm.read_729())


