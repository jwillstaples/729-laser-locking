# config stores constants necessary for lock_main
# July 16, 2021

# testing2

# PID constants
CONST_P = 1
CONST_I = 0.1
CONST_D = 0.01

# linear coarse adjust consant
CONST_K = 3

# wait times
FINE_WAIT = 0.01
COARSE_WAIT = 0.1

pid_sample_time = None  # should be < fine_wait

# WLM details
dll_path = "C:\Windows\System32\wlmData.dll"
ch_729 = 8

# ch3 lock split
locked = 4

# labjack addresses
name_ch2 = "AIN0"
name_ch3 = "AIN1"
name_dac = "DAC0"
