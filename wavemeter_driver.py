"""
NOTE : This driver should be running side by side with HighFinesse wavemeter software.
"""

import sys
import logging
from typing import Dict, Optional, Tuple

from ctypes import pointer, c_long

# wlmData.dll related imports
from . import wlmData
from . import wlmConst

__all__ = ['WavemeterDriver', 'StatusTag']

DEFAULT_DLL_PATH = "C:\\Program Files (x86)\\HighFinesse\\Wavelength Meter WS8 4043\\Projects\\64\\wlmData.dll"
"""Default DLL path."""


class WavemeterDriver:
    """ driver for HighFinesse wavemeter WS-8 """

    CHANNEL_NUM: int = 8  # WS-8 support 8-ch fiber switches

    # noinspection PyPep8Naming
    def __init__(self, dll_path: Optional[str] = None) -> None:
        if dll_path is None:
            # Load default DLL path
            dll_path = DEFAULT_DLL_PATH

        # Load DLL from DLL_PATH
        try:
            wlmData.LoadDLL(dll_path)
        except FileNotFoundError:
            sys.exit(f"Error: Could not find DLL on path {dll_path}")

        # Checks the number of WLM server instance(s)
        if wlmData.dll.GetWLMCount(0) == 0:
            print("There is no running wlmServer instance(s).")
            raise Exception('No running wlmServer found')
        else:
            # Read Type, Version, Revision and Build number
            Version_type = wlmData.dll.GetWLMVersion(0)
            Version_ver = wlmData.dll.GetWLMVersion(1)
            Version_rev = wlmData.dll.GetWLMVersion(2)
            Version_build = wlmData.dll.GetWLMVersion(3)
            print("WLM Version: [%s.%s.%s.%s]" % (Version_type, Version_ver, Version_rev, Version_build))

        self.logger = logging.getLogger(__name__)

    def get_temperature(self) -> Optional[float]:
        """ Return wavemeter temperature"""
        temperature = wlmData.dll.GetTemperature(0.0)
        if temperature <= wlmConst.ErrTemperature:
            self.logger.debug("Temperature: Not available")
            return None
        else:
            self.logger.debug(f"Temperature: {temperature:.1f} °C")
            return temperature

    def get_pressure(self) -> Optional[float]:
        """ Return wavemeter pressure """
        pressure = wlmData.dll.GetPressure(0.0)
        if pressure <= wlmConst.ErrTemperature:
            self.logger.debug("Pressure: Not available")
            return None
        else:
            self.logger.debug(f"Pressure: {pressure:.1f} mbar")
            return pressure

    def get_frequency_all(self) -> Tuple[Dict[int, float], Dict[int, str]]:
        """
        retrieve frequency of all channels
        and return dictionary of frequency and status

        NOTE: This method does NOT trigger wavemeter frequency readout.
        It only parse the data from the latest measurement
        which are stored in dll file of the wavemeter software

        :return: retrieved frequency (in MHz) and channel status
        """
        frequency = {}
        status = {}
        for channel in range(1, 9):
            f, s = self.get_frequency(channel)
            frequency[channel] = f
            status[channel] = s

        return frequency, status

    def get_frequency(self, channel: int) -> Tuple[float, str]:
        """
        retrieve frequency of the specified channel
        and return frequency and status

        NOTE: This method does NOT trigger wavemeter frequency readout.
        It only parse the data from the latest measurement
        which are stored in dll file of the wavemeter software

        :params channel: fiber switch channel (1 ~ 8)
        :return: retrieved frequency (in MHz) and channel status
        """
        use, view = self.get_switch_state(channel)
        temp = wlmData.dll.GetFrequencyNum(channel, 0)
        if use:
            frequency = 0.0
            if temp == wlmConst.ErrNoValue:
                status = StatusTag.NoValue
            elif temp == wlmConst.ErrNoSignal:
                status = StatusTag.NoSignal
            elif temp == wlmConst.ErrLowSignal:
                status = StatusTag.LowSignal
            elif temp == wlmConst.ErrBigSignal:
                status = StatusTag.HighSignal
            else:
                status = StatusTag.Updated
                frequency = float(temp * 10 ** 6)
        else:
            frequency = 0.0
            status = StatusTag.Off

        return frequency, status

    def get_switch_state(self, channel: int) -> Tuple[int, int]:
        """
        return the fiber switch status of the specified channel

        :param channel: fiber switch channel (1 ~ 8)
        :return: fiber switch status (boolean) and interferogram display status (boolean)
        """
        use = c_long(0)
        view = c_long(0)
        use_p = pointer(use)
        view_p = pointer(view)

        wlmData.dll.GetSwitcherSignalStates(channel, use_p, view_p)
        self.logger.debug(f'channel {channel} switch status : {use.value}')
        self.logger.debug(f'channel {channel} display status : {view.value}')

        return use.value, view.value

    def set_switch_state(self, channel: int, use: bool, view: bool = False) -> None:
        """
        toggle the fiber switch status of the specified channel

        :param channel: fiber switch channel (1 ~ 8)
        :param use: desired fiber switch status (boolean)
        :param view: desired interferogram display status.
        The interferogram is only visible in HighFinesse wavemeter software
        """
        wlmData.dll.SetSwitcherSignalStates(channel, use, view)
        self.logger.debug(f'channel {channel} switch status set to {use}')

    # noinspection PyMethodMayBeStatic
    def start_measurement(self):
        """ start wavemeter reading """
        wlmData.dll.Operation(wlmConst.cCtrlStartMeasurement)

    # noinspection PyMethodMayBeStatic
    def stop_measurement(self):
        """ stop wavemeter reading """
        wlmData.dll.Operation(wlmConst.cCtrlStopAll)

    def calibrate_wavemeter(self, channel: int, calibration_freq: float):
        """
        calibrate wavemeter with a laser with well-known frequency standard.
        This function doesn't work if auto-calibration function is on.

        :param channel: calibration channel number
        :param calibration_freq: known frequency of the calibration laser in MHz

        """
        if wlmData.dll.GetAutoCalMode(0):
            self.logger.warning('No manual wavemeter calibration is available while autocalibration is on')
        else:
            # stop measurement before calibration
            self.stop_measurement()
            wlmData.dll.Calibration(wlmConst.cOther,
                                    wlmConst.cReturnFrequency,
                                    calibration_freq / (10 ** 6),  # convert MHz into THz
                                    channel)
            self.start_measurement()


class StatusTag:
    """ Wavemeter channel status tags """
    NoValue = 'No Value'
    NoSignal = 'No Signal'
    LowSignal = 'Low Power'
    HighSignal = 'High Power'
    Updated = 'Updated'
    Off = 'Switch Off'
