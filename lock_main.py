### lock_main.py intended to implement logic for maintaing 729nm frequency with noise
### author Will Staples
### July 7, 2021

from simple_pid import PID
from labjack import ljm
from time import sleep
from wavemeter_driver import WavemeterDriver
import config

class LockAssistant:

    def __init__(self, ref_freq):

        self.ref_freq = ref_freq

        self._CONST_P = config.CONST_P
        self._CONST_I = config.CONST_I
        self._CONST_D = config.CONST_D
        self.pid = PID(self.CONST_P, self.CONST_I, self.CONST_P, sample_time=config.pid_sample_time)

        self._CONST_K = config.CONST_K

        self.FINE_WAIT = config.FINE_WAIT
        self.COARSE_WAIT = config.COARSE_WAIT

    def set_ref(self, new_ref: float) -> None:
        self.ref_freq = new_ref

    def reset_pid(self) -> None:
        self.pid = PID(self._CONST_P, self._CONST_I, self._CONST_D)

    def fine_adjust(self, error: float) -> float:
        return self.pid(error)

    def locked(self, ch3: float) -> bool:
        return ch3 > config.locked

    def coarse_adjust(self, frequency: float) -> float:
        return self._CONST_K * (self.ref_freq - frequency)

class LabjackMonitor:

    def __init__(self):

        self.handle = ljm.openS("T4", "USB", "ANY")

        self._name_ch2 = config.name_ch2
        self._name_ch3 = config.name_ch3
        self._name_dac = config.name_dac

    def read_ch2(self) -> float:
        return ljm.eReadName(self.handle, self._name_ch2)

    def read_ch3(self) -> float:
        return ljm.eReadName(self.handle, self._name_ch3)

    def write_dac(self, val: float) -> None:
        ljm.eWriteName(self.handle, self._name_dac, val)


class WlmMonitor:

    def __init__(self):

        self.ch_729 = config.ch_729

        self._ws7 = WavemeterDriver(dll_path=config.dll_path)

    def read_729(self) -> float:
        return self._ws7.get_frequency(self.ch_729)[0]


def main() -> None:

    # create wlm_monitor
    wm = WlmMonitor()

    # find initial reference
    ref_freq = wm.read_729()

    # create lock assistant
    la = LockAssistant(ref_freq)

    # create labjack monitor
    lm = LabjackMonitor()

    # initialize variables
    output = 0
    freq = 0
    ch2 = 0
    ch3 = lm.read_ch3()

    # monitor/response loop
    while True:

        # loop for maintaining using a pid on error from ch2
        while la.locked(ch3):

            ch2 = lm.read_ch2()
            output = la.fine_adjust(ch2)
            if output != 0:
                lm.write_dac(output)

            sleep(la.FINE_WAIT)
            ch3 = lm.read_ch3()

        # loop for re-locking using data from wavemeter and proportional response
        while not la.locked(ch3):

            freq = wm.read_729()
            output += la.coarse_adjust(freq)  # note that the coarse adjust is a delta, so we use += not =
            lm.write_dac(output)

            sleep(la.COARSE_WAIT)
            ch3 = lm.read_ch3()

        la.reset_pid()
        ref_freq = wm.read_729()
        la.set_ref(ref_freq)


if __name__ == "__main__":
    main()
